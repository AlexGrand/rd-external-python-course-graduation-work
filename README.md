# RD External Python Course Graduation Work ([PLAN](documentation/RD External Python Course Graduation Work.xlsx))

## Welcome to my external graduation work for EPAM.<br> Please feel yourself comfortable and don't hesitate to DM me ([Telegram](https://t.me/alex_grnd) :smiley:).
**Information about the project flow and it steps will be added just down here.**

## TABLE OF CONTENTS:
1. [Project Tasks and steps](#1-project-tasks-and-steps)
2. [APP Installation Steps](#2-app-installation-steps)
3. [CRUD Commands](#3-crud-commands)

### Project Tasks and steps


- [x] In the root of the repository create folder "documentation". Create there a document Software Requirements Specification (SRS) with a description of a software system to be developed with all applicable use cases. The specification document must include forms mockups too. SRS has to be created in Markdown format in English.<br>
**RESULT**:<br>
[Software Requirements Specification](documentation/Specification.md)<br>
- [x] Create a static mockup version of the application in HTML format with hardcoded data. It must include a minimum of 4 pages: departments.html, department.html, employees.html, and employee.html. All pages have to include hyperlinks to simulate application use cases.
Inside ""documentation"" folder create folder ""html_prototype"" and push static prototype there.\
**RESULT:**\
Added static html mockups for web service (without css for now)\
[Telsa Inc - Departments](https://alexgrand.gitlab.io/rd-external-python-course-graduation-work/documentation/htmlPrototype/)<br>

- [x] Choose the project type: Flask application\
Choose Python technologies for working with database: SQLAlchemy\
**RESULT:**\
The next stack of technologies was chosen:\
`Flask` - webframework for fast web development\
`Flask-sqlalchemy` - for working with DB in ORM style\
`Flask-restful` - for REST approach in app\
`Unittest` - for testing\
`Flask-migrate` -for db migration<br>

- [x] "Create a Python project with the following required structure:\
department-app (a project / app directory)\
|__ migrations (this package must include migration files to manage database schema changes )\
|__ models (this package must include modules with Python classes describing DB models (for ORM only))\
|__ service (this package must include modules with functions / classes to work with DB (CRUD operations))\
|__ sql (this folder must include *.sql files to work with DB (for non-ORM only))\
|__ rest (this package must include modules with RESTful service implementation)\
|__ templates (this folder must include web app html templates)\
|__ static (this folder must include static files (js, css, images, etc,))\
|__ tests (this package must include modules with unit tests)\
|__ views (this package must include modules with Web controllers / views)\
NOTES:
Project must include setup.py file - installation file telling Python how to install your project.
Any other project files / packages / sub-packages you might add in the future."\
**RESULT:**\
`Setup.py` file was made in declarative style:\
[setup config](/telsa_management_api/setup.cfg)\
[setup.py](/telsa_management_api/setup.py)

- [x] Add the following to the project configuration:\
Project build configuration should include “pylint” plugin\
Use https://travis-ci.com for building the project on github\
Set up and add https://coveralls.io<br>
**RESULT:**\
Gitlab-CI was used instead of travis-ci in this project.\
Pylint and Pycodestyle were added as Static Analyse test stage.\
Coverage and Coveralls were used for unittests.\
[Config file for gitlab-CI](.gitlab-ci.yml)\
[Coveralls link](https://coveralls.io/gitlab/AlexGrand/rd-external-python-course-graduation-work)

- [x] "Create database with the following requirements:
Create two tables: “department” and “employee”
Populate database with the test data
Departments should store their names
Employees should store the following data: related department, employee name, date of birth, salary\
NOTES:\
Configure your application to connect to the required db.
If you chosen ORM technology to work with db (SQLAlchemy), you should create models and then generate migration scripts to manage database schema changes . You should use special Python modules or corresponding features of the chosen technology to generate migration scripts automatically or manually based on created modules.\
If you chosen non-ORM technology to work with db (mysql-connector-python), you should use special Python modules to run migration scripts which you should create manually.\
**RESULT:**\
`Flask-sqlalchemy` was chosen to work with database in ORM style.\
Were created 2 models to work with database: `Employee` and `Department`:\
[Employee model](/telsa_management_api/models/employee.py) |
[Department model](/telsa_management_api/models/department.py)\
Migrations are made by `flask db migrate` and `flask db upgrade` commands from CLI.


- [x] Create a web service (RESTful) for CRUD operations. One should be able to deploy the web service on Gunicorn using command line.  All public functions / methods on all levels should include unit tests. Debug information should be displayed at the debugging level in the console and in a separate file. Classes and functions / methods must have docstrings comments.\
**RESULT:**
1. CRUD operations are done with `flask_restful.Resource` and are avalable through `/api/` root url. Examples of usage are given in APP installation section.\
Source code:\
[Employee CRUD](telsa_management_api/rest/employee.py) |
[Department CRUD](telsa_management_api/rest/department.py)
2. All functions include unittests and docstring comments. APP is 100% covered by unittests:\
[Pylint, PEP8, Unittest results](https://asciinema.org/a/u5okHB0eaCdviFtVk5xywNIOY)
3. Debug is done by python's `logging` module. All errors or warnings that cause `500 Internal Server Error` are saved into `app.log` file.

- [x] Create a simple web application for managing departments and employees. The web application should use aforementioned web service for storing  data and reading from database.  One should be able to deploy the web application on Gunicorn using command line.  All public functions / methods on all levels should include unit tests. Debug information should be displayed at the debugging level in the console and in a separate file. Classes and functions / methods must have docstrings comments. Finalyze README file which should contain a brief description of the project, instructions on how to build a project from the command line, how to start it, and at what addresses the Web service and the Web application will be available after launch.\
The web application should allow:\
display a list of departments and the average salary (calculated automatically) for these departments
display a list of employees in the departments with an indication of the salary for each employee and a search field to search for employees born on a certain date or in the period between dates
change (add / edit / delete) the above data\
NOTE:\
This step may require updating existing or adding new REST endpoints to the aforementioned web service (if they were not taken into account in the previous step). For example, the implementation of employee search by date of birth or the addition of the possibility of calculating the average salary when getting a list of departments.\
**RESULT:**\
All done.



### 2. APP Installation Steps
1. Install `Ubuntu 20.04` on your local machine or VPS
2. Install Python3.8+ ([source](https://www.python.org/)), pip, and venv
3. Update your system:
```bash
sudo apt update
``` 
4. Install MySQL:
```bash
sudo apt install mysql-server mysql-client
```
5. Check if MySQL is installed:
```bash
mysql -V
```
6. Start MySQL Server:
```bash
sudo service mysql start
```
7. Login into MySQL as root
```bash
sudo mysql -u root
```
8. Create Database and User, grant User privileges.
```bash
CREATE DATABASE telsa_management_api;
CREATE USER 'telsa_management_api'@'localhost' IDENTIFIED BY 't3ls4_m4n4g3m3nt_4p1';
GRANT ALL PRIVILEGES ON telsa_management_api.* to 'telsa_management_api'@'localhost';
exit;
```
Default db name, username and password are hold in [`config.py`](telsa_management_api/config.py) file inside `telsa_management_api` folder. If you want to change user, password and db name in MySQl please don't forget to change it also in `config.py` file and  `SQLALCHEMY_DATABASE_URI` variable.

9. Create folder that will hold your app:
```bash
mkdir app
cd app
```
10. Install git to your system
```bash
sudo apt install git
```

11. Clone or fork project from gitlab
```bash
git clone https://gitlab.com/AlexGrand/rd-external-python-course-graduation-work.git
```

12. Create virtual environment inside `telsa_management_api` folder
```bash
cd rd-external-python-course-graduation-work/telsa_management_api
python3 -m venv venv
```

13. Enter into your virtual environment and install app
```bash
source venv/bin/activate
pip install wheel
pip install .
```

14. Create tables in your db with alembic by migrate command
```bash
flask db init
flask db migrate
flask db upgrade
```

15. Start your gunicorn server
```bash
gunicorn --workers=1 'app:create_app()'
```

16. Follow default host with port :8000.\
`note:` if you are on remote machine use your host instead of localhost.
The app is available on:
```bash
http://localhost:8000
```

17. Enjoy!:metal:


### 3. CRUD Commands

1. GET list of all departments.
```bash
curl -X GET localhost:8000/api/departments
```
2. POST new department:
```bash
curl -i -X POST localhost:8000/api/departments -H "Content-Type: application/json" -d '{"Name":"Sales Department"}'
```

3. GET department's data. 
`note:` Provide department's name instead of `<string:department_name>`
```bash
curl -X GET localhost:8000/api/departments/<string:department_name>
```

4. PUT request to edit department\
`note:` You can edit only department's name. Average salary and Employees are calculated automatically\
`note:` Fields that you don't provide or leave empty are left unchanged.\
`note:` Provide department's name instead of `<string:department_name>`
```bash
curl -i -X PUT localhost:8000/api/departments/<string:department_name> -H "Content-Type: application/json" -d '{"Name":"Sales Department"}'
```

5. DELETE Department\
`note:` Provide department's name instead of `<string:department_name>`
```bash
curl -i -X DELETE localhost:8000/api/departments/<string:department_name> -H "Content-Type: application/json" -d '{}'
```

6. GET list of Employees
```bash
curl -X GET localhost:8000/api/employees
```

7. POST new Employee\
`note:` Change data for your own.
```bash
curl -i X POST localhost:8000/api/employees -H "Content-Type: application/json" -d '{
    "Firstname":"Test",
    "Lastname":"Test",
    "Birthdate":"20000101",
    "Salary":18000,
    "Department":"Sales Department"
    }'
```

8. GET Employee\
`note:` Provide employee's name instead of `<string:first_name>_<string:last_name>`
```bash
curl -i X GET localhost:8000/api/employees/<string:first_name>_<string:last_name>
```

9. PUT request to edit Employee\
`note:` Provide employee's name instead of `<string:first_name>_<string:last_name>`
`note:` Fields that you don't provide or leave empty are left unchanged.
```bash
curl -i -X PUT localhost:8000/api/employees/Test_Test -H "Content-Type: application/json" -d '{"Firstname": "Alex"}'
```

10. DELETE Employee\
`note:` Provide employee's name instead of `<string:first_name>_<string:last_name>`
```bash
curl -i -X DELETE localhost:8000/api/employees/Test_Test -H "Content-Type: application/json" -d '{}'
```

11. GET employees by birthdate\
`note:` Provide employee's name instead of `<string:birthdate_start>-<string:birthdate_end>`
```bash
curl -i -X GET localhost:8000/api/employees/by_birthdate/<string:birthdate_start>-<string:birthdate_end>
```

12. GET employees in department\
`note:` Provide department's name instead of `<string:department_name>`
```bash
curl -i -X GET localhost:8000/api/employees/department/<string:department_name>
```