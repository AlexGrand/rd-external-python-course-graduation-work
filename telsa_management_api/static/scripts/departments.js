import {urls} from './urls.js';
import {fetch_data, get_data, open_url} from './utils.js';
import {render_table, render_button, render_url_link, render_search_form} from './render.js';

let section_el;
let title_name;


function render_page(data) {
    render_html(data);
    add_events();
}

function render_html(data) {
    if (data.length > 0) {
        let add_search_el = render_search_form('search', 'search_form', [
            {"field_name":'Department', "type":"search"}
        ]);
        let table_el = render_table(data);
        section_el.appendChild(add_search_el);
        section_el.appendChild(table_el);
    }

    let add_dep_button = render_button('add department', 'add_department', 'btn btn-info btn-block');
    section_el.appendChild(add_dep_button);
    add_links();
}

function add_events() {
    section_el.addEventListener('click', (e) => {
        if (e.target.tagName == 'BUTTON') {
            let elem = e.target;
            if (elem.value == 'delete') del_department(e);
            if (elem.value == 'edit') edit_department(e);
            if (elem.value == 'add_department') add_department();
        }
    });
    // add event for search button
    let search_form_el = section_el.querySelector('.search_form')
    if (search_form_el) {
        search_form_el.addEventListener('submit', search_department);
    }
}

function del_department(e) {
    let parent_el = e.target.parentNode.parentNode;
    let first_col = parent_el.firstChild;
    let dep_name = first_col.innerText;
    let field_name = first_col.getAttribute('key');
    let confirmation = confirm(
        "Are you sure you want to delete "+dep_name+'?'
    );
    if (confirmation) {
        let data = {};
        data[field_name] = dep_name;
        data = JSON.stringify(data);
        fetch_data(
            'delete', urls.api.department+dep_name, data, () => {
                open_url(urls.root);
            });
    }
}

function add_department() {
    open_url(urls.add_department);
}

function edit_department(e) {
    let row_el = e.target.parentNode.parentNode;
    let dep_name = row_el.firstChild.innerText;
    open_url(urls.edit_department + dep_name);
}

function add_links () {
    let dep_name_els = section_el.querySelectorAll("td[key='Name']");
    dep_name_els.forEach(element => {
        let dep_name = element.innerText;
        render_url_link(element, urls.department+dep_name);
    });
}

function search_department(e) {
    let formData = new FormData(e.target);
    let dep_name = formData.get('Department');
    get_data(urls.api.department_employees+dep_name, () => {
        open_url(urls.department+dep_name);
    });
        
    e.preventDefault();
}


get_data(urls.api.departments, (result) => {
    render_page(result.data)
});

document.addEventListener("DOMContentLoaded", () => {
    section_el = document.querySelector('.content');
    title_name = section_el.querySelector('h1').innerText;
});