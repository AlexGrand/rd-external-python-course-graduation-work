import {urls} from './urls.js';
import {sendFormData, get_data, open_url} from './utils.js';
import {render_form} from './render.js';

let section_el;
let title_name;
let path;


function render_page(data) {
    render_html(data);
    add_events();
}

function render_html(data) {
    let form_el = render_form(data);
    section_el.appendChild(form_el);
    let department_input_el = section_el.querySelector('input[name="Department"]')
    department_input_el.removeAttribute('required')

    edit_birthdate_field();
}

function add_events() {
    let form_el = section_el.querySelector('form');
    form_el.addEventListener('submit', edit_employee);
}

function edit_employee(e) {
    let form_el = e.target;
    sendFormData(form_el, 'PUT', urls.api.employee+path, () => {
        open_url(urls.employees);
    });
    e.preventDefault();
}

function edit_birthdate_field() {
    let birthdate_el = section_el.querySelector('input[name="Birthdate"]');
    birthdate_el.setAttribute("type","date");
}

document.addEventListener("DOMContentLoaded", () => {
    section_el = document.querySelector('.content');
    title_name = section_el.querySelector('h1').innerText;
    path = title_name.split(' ').join('_');
    get_data(urls.api.employee + path, render_page);
});