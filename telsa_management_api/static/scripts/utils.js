function create_html(el, text='', args_list=null) {
    let element = document.createElement(el);
    element.innerText = text;
    if (args_list !== null) {
        for (let i=0; i<args_list.length; i++) {
            let attrs = args_list[i];
            let key = Object.keys(attrs)[0];
            element.setAttribute(key, attrs[key]);
        };
    };
    return element;
}

async function get_data(url, processing_func) {
    let response = await fetch(url);
    if (response.ok) {
        let result = await response.json();
        processing_func(result);
    } else {
        let result = await response.json();
        alert("ERROR: "+result.error);
    }
}

async function fetch_data(method, url, data, processing_func) {
    let response = await fetch(url, {
    method: method,
    headers: {
        'Content-Type': 'application/json;'
    },
    body: data
    });
    if (response.ok) {
        if (response.status == 204) {
            processing_func();
            return;
        }
        let result = await response.json();
        processing_func(result);
    } else {
        let result = await response.json();
        alert("ERROR: " + result.error);
    }
}

function sendFormData(form_el, method, url, processing_func) {
    let formData = new FormData(form_el);
    let data = {};
    for (let tuple of formData.entries()) {
        let key = tuple[0];
        let value = tuple[1];
        data[key] = value;
    }
    fetch_data(
        method,
        url,
        JSON.stringify(data),
        processing_func
    );
}

function render_Formdata(data) {
    let formData = new FormData()
    if (data) {
        for (let key in data) {
            formData.append(key, data[key]);
        }
    }
    return formData;
}

function open_url(url) {
    window.open(url, name="_self");
}



export {create_html, get_data, fetch_data, render_Formdata, open_url, sendFormData}