let urls = {
    "root": "/",
    "employees": "/employees",
    "department": "/departments/",
    "employee": "/employees/",
    "add_department": "/departments/add_department/",
    "edit_department": "/departments/edit_department/",
    "add_employee": "/employees/add_employee/",
    "edit_employee": "/employees/edit_employee/",
    "employees_by_birthdate": "/employees/by_birthdate/"
};

urls['api'] = {
    "departments": "/api/departments",
    "department": "/api/departments/",
    "employees": "/api/employees",
    "employee": "/api/employees/",
    "by_birthdate": "/api/employees/by_birthdate/",
    "department_employees":"/api/employees/department/"
};

export {urls};