import {urls} from './urls.js';
import {sendFormData, open_url} from './utils.js';

let section_el;


function render_page() {
    add_events();
}

function collect_form_data(e) {
    sendFormData(e.target, 'POST', urls.api.employees, () => {
        open_url(urls.employees);
    });
    e.preventDefault();
}


function add_events() {
    let form_el = section_el.querySelector('form');
    form_el.addEventListener('submit', collect_form_data);
}

document.addEventListener("DOMContentLoaded", () => {
    section_el = document.querySelector('.content');
    render_page();
});