import {create_html} from './utils.js';

function render_table(data) {
    //  if no data - exit
    if (!data) {
        return create_html('h3', 'No data yet.');
    }

    if ('data' in data) {
        data = data.data;
    }
    let table_el = create_html('table', '', [{"class":"table table-hover"}]);
    let thead_el = create_html('thead');
    let tbody_el = create_html('tbody');
    let headers = render_headers(data);

    if (data.length > 0) {
        for (let i=0; i < data.length; i++) {
            let row_el = render_row(data[i]);
            tbody_el.appendChild(row_el)
        }
    } else {
        let row_el = render_row(data);
        tbody_el.appendChild(row_el);
    }

    thead_el.appendChild(headers);
    table_el.appendChild(thead_el);
    table_el.appendChild(tbody_el);
    return table_el;
}

function render_headers(data) {
    if (data.length > 0) data = data[0];
    let headers = Object.keys(data);
    let tr_el = create_html('tr', '', [{"class":"bg-secondary text-white"}]);
    for (let i=0; i<headers.length; i++) {
        let th_el = create_html('th', headers[i], [{"class":"text-center"}]);
        tr_el.appendChild(th_el);
    }
    tr_el.appendChild(create_html('th', 'Options'));
    return tr_el;
}

function render_row(data) {
    let tr_el = create_html('tr');
    let td_el;
    for (let key in data) {
        td_el = create_html(
            'td', data[key], [{"key": key}, {"class":"text-center"}]);
        tr_el.appendChild(td_el);
    }
    let edit_button = render_button('edit', 'edit', 'btn btn-warning');
    let delete_button = render_button('delete', 'delete', 'btn btn-danger');
    td_el = create_html('td');
    td_el.appendChild(edit_button);
    td_el.appendChild(delete_button);
    tr_el.appendChild(td_el);
    return tr_el;
}

function render_button(name=null, value=null, cls=null) {
    let attrs = [];
    if (cls) attrs.push({"class":cls});
    if (value) attrs.push({"value":value});
    let button = create_html(
        'button', name, attrs);
    return button;
}

function render_form(data) {
    let form_el = create_html('form', '', [{"class":"input_form col-12"}]);
    let row_el;
    for (let key in data) {
        row_el = create_html('div', '', [{"class":"form-row"}]);
        let col_el = create_html('div','',[{"class":"col-2"}]);
        let label_el = create_html('label', key, [{"for":key}]);
        col_el.appendChild(label_el);
        row_el.appendChild(col_el);

        col_el = create_html('div','',[{"class":"col-4"}]);
        let input_el = create_html('input','', [
            {"class":"form-control"}, {"type":"text"}, {"required":"True"},
            {"name":key}, {"value": data[key]}
        ]);
        col_el.appendChild(input_el);
        row_el.appendChild(col_el);

        form_el.appendChild(row_el);
    }

    
    
    let submit_el = create_html('button', 'Submit', [
        {"type": 'submit'}, {"class":"btn btn-info"}
    ]);
    form_el.appendChild(submit_el);
    return form_el;
}

function render_url_link(node, url) {
    let text = node.innerText;
    node.innerText = '';
    let a_el = create_html('a', text, [{"href": url}]);
    node.appendChild(a_el);
}

function render_search_form(btn_name, cls_name, fields=[]) {
    let form_el = create_html('form', '', [{"class": cls_name}]);
    let row_el = create_html('div','', [{"class":"form-row"}]);
    let br_el = create_html('br');
    for (let i=0; i<fields.length; i++) {
        let field_name = fields[i]['field_name'];
        let input_type = fields[i]['type'];
        let col_el = create_html('div', '', [{"class":"col"}]);
        let input_el = create_html('input', '', [
            {"name":field_name}, {"type":input_type}, {"required": "True"},
            {"placeholder":field_name}, {"class":"form-control"}
        ]);
        col_el.appendChild(input_el);
        row_el.appendChild(col_el);
    }
    row_el.appendChild(create_html(
        'button', btn_name, [{"type":"submit"}, {"class":"btn btn-info"}]
        ));
    
    form_el.appendChild(row_el);
    form_el.appendChild(br_el);
    return form_el;
}


export {
    render_table,
    render_row,
    render_headers,
    render_button,
    render_form,
    render_url_link,
    render_search_form
}