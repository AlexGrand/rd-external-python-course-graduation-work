import {urls} from './urls.js';
import {get_data, fetch_data, open_url, create_html} from './utils.js';
import {render_table, render_button, render_url_link, render_search_form} from './render.js';

let section_el;
let title_name;


function render_page(data) {
    render_html(data);
    add_events();
}

function render_html(data) {
    if (data.length > 0) {
        let add_search_el = render_search_form('search', 'search_employee', [
            {"field_name":"Firstname", "type":"search"},
            {"field_name":"Lastname", "type":"search"}
        ]);
        let add_search_birthdate_el = render_search_form(
            'search', 'search_by_birthdate', [
                {"field_name":'Min birthdate',"type":"date"},
                {"field_name":'Max birthdate',"type":"date"}
        ]);
        let table_el = render_table(data);
        section_el.appendChild(add_search_el);
        section_el.appendChild(add_search_birthdate_el);
        section_el.appendChild(table_el);
    }
    
    let add_empl_button = render_button('add employee', 'add_employee', 'btn btn-info btn-block');
    section_el.appendChild(add_empl_button);
    add_links();
}

function add_links() {
    add_department_links();
    add_employee_links();
}

function add_department_links() {
    let dep_name_els = section_el.querySelectorAll("td[key='Department']");
    dep_name_els.forEach(element => {
        let dep_name = element.innerText;
        render_url_link(element, urls.department+dep_name);
    });
}

function add_employee_links() {
    let row_els = section_el.querySelectorAll("tbody > tr");
    row_els.forEach(element => {
        let first_name_el = element.querySelector("td[key='Firstname']");
        let last_name_el = element.querySelector("td[key='Lastname']");
        let path = urls.employee+first_name_el.innerText+' '+last_name_el.innerText;
        render_url_link(first_name_el, path);
        render_url_link(last_name_el, path);
    });
}

function add_events() {
    section_el.addEventListener('click', (e) => {
        if (e.target.tagName == 'BUTTON') {
            let elem = e.target;
            if (elem.value == 'delete') del_employee(e);
            if (elem.value == 'edit') edit_employee(e);
            if (elem.value == 'add_employee') add_employee();
        }
    });
    // add event for search button
    let search_form_el = section_el.querySelector('.search_employee')
    if (search_form_el) {
        search_form_el.addEventListener('submit', search_employee);
    }

    // search event for search by birthdate
    let search_by_birthdate_form = section_el.querySelector('.search_by_birthdate');
    if (search_by_birthdate_form) {
        search_by_birthdate_form.addEventListener('submit', search_by_birthdate);
    }
}

function del_employee(e) {
    let row_el = e.target.parentNode.parentNode;
    let first_name_el = row_el.querySelector("td[key='Firstname']");
    let last_name_el = row_el.querySelector("td[key='Lastname']");
    let fullname = first_name_el.innerText+' '+last_name_el.innerText;
    let confirmation = confirm(
        "Are you sure you want to delete "+fullname+'?'
    );
    if (confirmation) {
        fetch_data(
            'delete', urls.api.employee+first_name_el.innerText+'_'+last_name_el.innerText, '', () => {
                open_url(urls.employees);
            });
    }
}

function add_employee() {
    open_url(urls.add_employee);
}

function edit_employee(e) {
    let row_el = e.target.parentNode.parentNode;
    let first_name_el = row_el.querySelector("td[key='Firstname']");
    let last_name_el = row_el.querySelector("td[key='Lastname']");

    open_url(urls.edit_employee+first_name_el.innerText+' '+last_name_el.innerText);
}

function search_employee(e) {
    let formData = new FormData(e.target);
    let firstname = formData.get('Firstname');
    let lastname = formData.get('Lastname');
    let full_name = firstname+'_'+lastname;
    get_data(urls.api.employee+full_name, () => {
        open_url(urls.employee+firstname+' '+lastname);
    });
        
    e.preventDefault();
}

function search_by_birthdate(e) {
    let formData = new FormData(e.target);
    let min_date = formData.get('Min birthdate').match(/\d+/g);
    let max_date = formData.get('Max birthdate').match(/\d+/g);
    if (!min_date || !max_date) {
        alert("ERROR: birthdate must be in 'year-month-day' pattern!");
        return;
    } else {
        min_date = min_date.join('');
        max_date = max_date.join('');
    }
    let birthdate_range = min_date+'-'+max_date;
    console.log(birthdate_range);
    get_data(urls.api.by_birthdate+birthdate_range, (result) => {
        if (result.data.length == 0) {
            alert("There are no employees born during this birthdate range!");
            return;
        } else {
            open_url(urls.employees_by_birthdate+birthdate_range);
        }
    });

    e.preventDefault();
}

get_data(urls.api.employees, (result) => {
    render_page(result.data)

});

document.addEventListener("DOMContentLoaded", () => {
    section_el = document.querySelector('.content');
    title_name = section_el.querySelector('h1').innerText;
});