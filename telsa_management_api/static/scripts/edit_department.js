import {urls} from './urls.js';
import {sendFormData, get_data, open_url} from './utils.js';
import {render_form} from './render.js';

let section_el;
let title_name;


function render_page(data) {
    render_html(data);
    add_events();
}

function render_html(data) {
    let form_el = render_form(data);
    section_el.appendChild(form_el);
}

function add_events() {
    let form_el = section_el.querySelector('form');
    form_el.addEventListener('submit', edit_department);
}

function edit_department(e) {
    let form_el = e.target;
    sendFormData(form_el, 'PUT', urls.api.department+title_name, () => {
        open_url(urls.root);
    });
    e.preventDefault();
}


document.addEventListener("DOMContentLoaded", () => {
    section_el = document.querySelector('.content');
    title_name = section_el.querySelector('h1').innerText;
    get_data(urls.api.department + title_name, render_page);
});