"""API module that gets requests and sends response
for CRUD operations on Employee model """
from http import HTTPStatus
from re import sub
from datetime import date
# third
from flask import request
from flask_restful import Resource
# own
from utils.utils import is_wrong_birthdate
from models.department import Department
from models.employee import Employee


class EmployeesResource(Resource):
    """Resource that works with list of employees"""
    def get(self):
        """Get list of departments"""
        employees = Employee.get_all()
        data = []
        for employee in employees:
            data.append(employee.data)
        return {'data': data}, HTTPStatus.OK

    def post(self):
        """Create new Employee.

        :first_name:-:str: - employee firstname
        :last_name:-:str: - employee last_name
        :salary:-:str: - employee salary
        :birthdate:-:str: employee birthdate in str format (year-month-day)
        :department:-:str: department name

        :return: - JSON or BAD_REQUEST"""
        json_data = request.get_json()
        if json_data is None:
            return {"error": "no data given"}, HTTPStatus.BAD_REQUEST

        data = {}
        data['Firstname'] = json_data.get('Firstname')
        data['Lastname'] = json_data.get('Lastname')
        data['Salary'] = json_data.get('Salary')
        data['Birthdate'] = json_data.get('Birthdate')
        data['Department'] = json_data.get('Department')

        # check if values were given and are not empty
        no_data_in_dict = [
            key for key, val in data.items() if val is None or not val]
        if any(no_data_in_dict):
            return {"error": f"there is no data in {no_data_in_dict}"},\
                HTTPStatus.BAD_REQUEST

        # check if employee exists
        if Employee.get_by_name(data['Firstname'], data['Lastname']):
            error = {
                "error": f"employee {data['Firstname']} {data['Lastname']} "
                "already exists"}
            return error, HTTPStatus.BAD_REQUEST

        # check if mistake in birthdate
        birthdate_error = is_wrong_birthdate(data['Birthdate'])
        if birthdate_error:
            return birthdate_error, HTTPStatus.BAD_REQUEST

        # check if department exists
        department = Department.get_by_name(data['Department'])
        if department is None:
            return {
                "error": f"There is no '{data['Department']}'"
                " in existing departments."
            }, HTTPStatus.NOT_FOUND

        # convert data and save to the database
        try:
            first_name = str(data['Firstname'])
            last_name = str(data['Lastname'])
            salary = float(data['Salary'])
            birthdate = str(sub(r'[^0-9]', '', str(data['Birthdate'])))
            # convert birthdate to datetime obj
            year = int(birthdate[:4])
            month = int(birthdate[4:6])
            day = int(birthdate[6:8])
            birthdate = date(year, month, day)

        except ValueError as err:
            return {
                "error": f"wrong data given: {err.args[0].split().pop()}"
            }, HTTPStatus.BAD_REQUEST

        employee = Employee(
            first_name=first_name,
            last_name=last_name,
            salary=salary,
            birthdate=birthdate,
            department_id=department.id
        )
        employee.save()

        # set employees amount to Department Model
        department.set_employees_amount()

        # set avrg_salary to Department Model
        department.set_avrg_salary()

        return employee.data, HTTPStatus.CREATED


class EmployeeResource(Resource):
    """Resource that works with employee.

    :first_name:-:str: - firstname in the employee's table
    :last_name:-:str: - lastname in the employee's table
    """
    def get(self, first_name, last_name):
        """Get employee if exists"""
        employee = Employee.get_by_name(
            first_name=first_name,
            last_name=last_name
        )
        if employee is None:
            return {
                "error": "there is no employee with name "
                f"{first_name} {last_name}"
            }, HTTPStatus.BAD_REQUEST
        else:
            return employee.data, HTTPStatus.OK

    def put(self, first_name, last_name):
        """Update Employee by given data.

        :first_name:-:str: - firstname in the employee's table
        :last_name:-:str: - lastname in the employee's table

        :return: - JSON or BAD_REQUEST"""
        json_data = request.get_json()
        if json_data is None:
            return {"error": "no data given"}, HTTPStatus.BAD_REQUEST

        # collect given data
        data = {}
        data['first_name'] = json_data.get('Firstname')
        data['last_name'] = json_data.get('Lastname')
        data['salary'] = json_data.get('Salary')
        data['birthdate'] = json_data.get('Birthdate')
        data['department_id'] = None

        # check if data correct
        # check if user exists
        employee = Employee.get_by_name(first_name, last_name)
        if employee is None:
            return {
                "error": f"there is no employee: {first_name} "
                f"{last_name}"}, HTTPStatus.NOT_FOUND

        # check if attempt to rename with name that exists
        employee_exist = Employee.get_by_name(
            first_name=data['first_name'],
            last_name=data['last_name']
        )
        if employee_exist and employee_exist != employee:
            return {"error": "employee with such name already exists"},\
                HTTPStatus.BAD_REQUEST

        # check if salary data type is ok
        if data['salary']:
            try:
                data['salary'] = float(data['salary'])
            except ValueError as err:
                return (
                    {"error": f"wrong data: {err.args[0].split().pop()}"},
                    HTTPStatus.BAD_REQUEST)

        # check if birtdhate data type is ok
        if data['birthdate']:
            birthdate_error = is_wrong_birthdate(data['birthdate'])
            if birthdate_error:
                return birthdate_error, HTTPStatus.BAD_REQUEST
            else:
                birthdate = str(sub(r'[^0-9]', '', str(data['birthdate'])))
                birthdate = date(
                    int(birthdate[:4]),
                    int(birthdate[4:6]),
                    int(birthdate[6:8])
                )
                data['birthdate'] = birthdate

        # check if department is ok
        if json_data.get('Department'):
            new_dep_name = json_data.get('Department')
            new_dep = Department.get_by_name(new_dep_name)
            if new_dep is None:
                return {
                    "error": f"There is no '{json_data.get('Department')}'"
                    " in existing departments."
                }, HTTPStatus.NOT_FOUND
            # if department exists
            prev_dep_id = employee.department_id
            data['department_id'] = new_dep.id

        # save updated employee
        for key, val in data.items():
            if val is not None:
                setattr(employee, key, val)
        employee.save()

        # update avrg_salary and employees_amount for Department
        department = Department.get_by_id(employee.department_id)
        if department:
            department.set_avrg_salary()
            department.set_employees_amount()

        # update prev avrg_salary and employees_amount if department changed
        if json_data.get('Department') and prev_dep_id:
            department = Department.get_by_id(prev_dep_id)
            department.set_avrg_salary()
            department.set_employees_amount()

        return employee.data, HTTPStatus.OK

    def delete(self, first_name, last_name):
        """Delete employee from Employee table.

        :first_name:-:str: - firstname in the employee's table
        :last_name:-:str: - lastname in the employee's table

        :return: NO_CONTENT if delete was successfull"""
        employee = Employee.get_by_name(
            first_name=first_name,
            last_name=last_name
        )
        if employee is None:
            return {
                "error": "there is no employee with name "
                f"{first_name} {last_name}"
            }, HTTPStatus.NOT_FOUND
        else:
            department = Department.get_by_id(employee.department_id)
            employee.delete()
            # recalculate deparment avrg_salary and employees_amount
            if department:
                department.set_employees_amount()
                department.set_avrg_salary()
            return {}, HTTPStatus.NO_CONTENT


class EmployeesByBirthdateResource(Resource):
    """Resource that works with list of employees
    searched by birthdates range"""
    def get(self, birthdate_start, birthdate_end):
        """Get list of employees born between birthdates.

        :birthdate_start:-:str: - string in year-month-day pattern
        :birthdate_endt:-:str: - string in year-month-day pattern"""
        birthdate_error_1 = is_wrong_birthdate(birthdate_start)
        birthdate_error_2 = is_wrong_birthdate(birthdate_end)

        if birthdate_error_1 or birthdate_error_2:
            return birthdate_error_1 or birthdate_error_2,\
                HTTPStatus.BAD_REQUEST

        bdate_start = sub(r'[^0-9]', '', str(birthdate_start))
        bdate_end = sub(r'[^0-9]', '', str(birthdate_end))

        # convert birthdates to datetime type
        bdate_start = date(
            int(bdate_start[:4]), int(bdate_start[4:6]), int(bdate_start[6:8]))
        bdate_end = date(
            int(bdate_end[:4]), int(bdate_end[4:6]), int(bdate_end[6:8]))

        employees = Employee.get_by_birthdate_range(
            bdate_start, bdate_end
        )

        data = []
        if employees:
            for employee in employees:
                data.append(employee.data)
        return {'data': data}, HTTPStatus.OK


class EmployeesDepartmentResource(Resource):
    """List of employees by Department name"""
    def get(self, department_name):
        """Get list of employees by department name.

        :department_name:-:str: department name

        :return: JSON or NOT_FOUND"""
        # check if department exists
        department = Department.get_by_name(name=department_name)
        if department is None:
            return {'error': 'Department not found'}, HTTPStatus.NOT_FOUND

        # collect employees data
        data = []
        for employee in department.employees:
            data.append(employee.data)
        return {"data": data}, HTTPStatus.OK
