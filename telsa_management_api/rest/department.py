"""API module that gets requests and sends response
for CRUD operations on Department model """
from http import HTTPStatus
# third
from flask_restful import Resource, request
# own
from models.department import Department


class DepartmentsResource(Resource):
    """Resource that works with list of departments"""
    def get(self):
        """Get list of departments"""
        departments = Department.get_all()
        data = []
        for department in departments:
            data.append(department.data)
        return {'data': data}, HTTPStatus.OK

    def post(self):
        """Create new department in Department's table"""
        json_data = request.get_json()
        if json_data is None:
            return {"error": "no data given."}, HTTPStatus.BAD_REQUEST

        # check if department's name was given
        department_name = json_data.get('Name')
        if not department_name:
            return {"error": "no department's name was given."},\
                HTTPStatus.BAD_REQUEST

        # check if department with such name exists
        department = Department.get_by_name(name=department_name)
        if department is not None:
            return {"error": f"{department_name} exists"},\
                HTTPStatus.BAD_REQUEST

        # save if passed check
        department = Department(name=department_name)
        department.save()
        return department.data, HTTPStatus.CREATED


class DepartmentResource(Resource):
    """Resource that works with single department"""
    def get(self, department_name):
        """Get department by it's name.

        :department_name:-:str: string with department name

        :return: JSON or NOT_FOUND if there is no department with given name"""
        department = Department.get_by_name(name=department_name)
        if department is None:
            return {'error': 'Department not found'}, HTTPStatus.NOT_FOUND

        return department.data, HTTPStatus.OK

    def put(self, department_name):
        """Update department with given name.

        :department_name:-:str: string with department name

        :return: JSON or BAD_REQIEST"""
        json_data = request.get_json()
        if json_data is None:
            return {"error": "no data given."}, HTTPStatus.BAD_REQUEST

        department = Department.get_by_name(name=department_name)
        if department is None:
            return {'error': 'Department not found'}, HTTPStatus.NOT_FOUND

        department_name = json_data.get('Name')
        # check if department with such name exists
        department_exists = Department.get_by_name(name=department_name)
        if department_exists is not None and\
                department.name != department_name:
            return {"error": f"{department_name} exists"},\
                HTTPStatus.BAD_REQUEST

        # save department name if given
        if department_name:
            department.name = department_name
            department.save()
        return department.data, HTTPStatus.OK

    def delete(self, department_name):
        """Update department with given name.

        :department_name:-:str: string with department name

        :return: NO_CONTENT or BAD_REQUEST if fails"""
        json_data = request.get_json()
        if json_data is None:
            return {"error": "no data given."}, HTTPStatus.BAD_REQUEST

        # check if department exists
        department = Department.get_by_name(name=department_name)
        if department is None:
            return {'error': 'Department not found'}, HTTPStatus.NOT_FOUND

        # clean employes department_id
        employees = department.employees
        for employee in employees:
            employee.department_id = None

        department.delete()
        return {}, HTTPStatus.NO_CONTENT
