"""Base Config Object for the Flask instance.
Sets DEBUG, SQLALCHEMY_DATABASE_URI, SQLALCHEMY_TRACK_MODIFICATIONS
configs"""


class Config:
    """Class that holds basic confif for FLASK instance"""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://telsa_management_api:'\
        't3ls4_m4n4g3m3nt_4p1@localhost/telsa_management_api'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
