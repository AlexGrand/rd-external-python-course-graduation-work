"""Flask application that controls Telsa Management API.

Public functions:
:create_app: - creates FLask instance
:register_extensions: - registrate extensions from extensions.py including
database"""
import logging
from logging import FileHandler
# third
from flask import Flask
from flask_migrate import Migrate
from flask_restful import Api
from extensions import db
# own
from config import Config
from rest.department import DepartmentsResource, DepartmentResource
from rest.employee import (
    EmployeesResource, EmployeeResource, EmployeesByBirthdateResource,
    EmployeesDepartmentResource)
# views
from views.departments import DepatmentsView
from views.employees import EmployeesView, EmployeesByBirthdateView
from views.employee import EmployeeView, AddEmployeeView, EditEmployeeView
from views.department import (
    DepatmentView, AddDepatmentView, EditDepartmentView)


def create_app():
    """Creates FLask instance, sets config, registers extensions
    and url routes.

    :return: - Flask instance"""
    new_app = Flask(__name__)
    start_logger(new_app)
    new_app.config.from_object(Config)
    register_extensions(new_app)
    register_crud_urls(new_app)
    register_views(new_app)
    new_app.app_context().push()
    return new_app


def start_logger(app_obj):
    """Logger creates logs and pass then to the errorlog.txt"""
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app_obj.logger.handlers = gunicorn_logger.handlers

    file_handler = FileHandler('app.log')
    file_handler.setLevel(gunicorn_logger.level)
    app_obj.logger.addHandler(file_handler)


def register_extensions(app_obj):
    """Initialize SQLAlchemy database, creates flask_migrate.Migrate
    instance"""
    db.init_app(app_obj)
    Migrate(app_obj, db)


def register_crud_urls(app_obj):
    """Register urls for CRUD operations
    URL for CRUD operations is /api"""
    api = Api(app_obj)
    api.add_resource(DepartmentsResource, '/api/departments')
    api.add_resource(
        DepartmentResource, '/api/departments/<string:department_name>')
    api.add_resource(EmployeesResource, '/api/employees')
    api.add_resource(
        EmployeeResource,
        '/api/employees/<string:first_name>_<string:last_name>')
    api.add_resource(
        EmployeesByBirthdateResource,
        '/api/employees/by_birthdate/<string:birthdate_start>-'
        '<string:birthdate_end>'
    )
    api.add_resource(
        EmployeesDepartmentResource,
        '/api/employees/department/<string:department_name>'
    )


def register_views(app_obj):
    """Register urls from website to view funcs"""
    views = {
        '/': DepatmentsView.as_view('/'),
        '/employees': EmployeesView.as_view('employees'),
        '/departments/<string:department_name>':
            DepatmentView.as_view('department'),
        '/employees/<string:employee_name>':
            EmployeeView.as_view('employee'),
        '/departments/add_department/':
            AddDepatmentView.as_view('add_department'),
        '/departments/edit_department/<string:department_name>':
            EditDepartmentView.as_view('edit_department'),
        '/employees/add_employee/':
            AddEmployeeView.as_view('add_employee'),
        '/employees/edit_employee/<string:employee_name>':
            EditEmployeeView.as_view('edit_employee'),
        '/employees/by_birthdate/<int:min_date>-<int:max_date>':
            EmployeesByBirthdateView.as_view('by_birthdate')
    }
    for key, val in views.items():
        app_obj.add_url_rule(key, view_func=val)
