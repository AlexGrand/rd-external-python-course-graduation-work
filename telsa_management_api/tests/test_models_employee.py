"""Test for SQLALCHEMY.Model instance - Department"""
import unittest
from datetime import datetime
# third
from sqlalchemy import create_engine
# own
from app import create_app, db
from models.department import Department
from models.employee import Employee


class EmployeeDepartmentTestCase(unittest.TestCase):
    """Tests for Employee model"""
    def setUp(self):
        """Initial setup that sets TEST config and path to sqlite3
        db in memory"""
        app = create_app()
        engine = create_engine('sqlite://')

        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = engine.url
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        """Clean database data"""
        for department in Department.get_all():
            department.delete()
        for employee in Employee.get_all():
            employee.delete()

        db.drop_all()
        db.create_all()

    def test_get_by_id(self):
        "Test get_by_id class method of Employee class"
        employee = Employee(
            first_name='Test',
            last_name='Test',
            salary=18000,
            birthdate=datetime(2000, 1, 1)
        )
        employee.save()

        self.assertEqual(Employee.get_by_id(employee.id), employee)

    def test_get_by_name(self):
        "Test get_by_name class method of Employee class"
        employee = Employee(
            first_name='Test',
            last_name='Test',
            salary=18000,
            birthdate=datetime(2000, 1, 1)
        )
        employee.save()

        self.assertEqual(
            Employee.get_by_name(
                employee.first_name, employee.last_name), employee
            )

    def test_get_by_birthdate_range(self):
        "Test get_by_birthdate_range class method of Employee class"
        employee = Employee(
            first_name='Test',
            last_name='Test',
            salary=18000,
            birthdate=datetime(2000, 1, 1)
        )
        employee.save()

        self.assertEqual(Employee.get_by_birthdate_range(
            datetime(1999, 1, 1), datetime(2001, 1, 1)
        )[0], employee)

    def test_data_property(self):
        "Test data class property of Employee class"
        department = Department(name='Test')
        department.save()

        employee = Employee(
            first_name='Test',
            last_name='Test',
            salary=18000,
            birthdate=datetime(2000, 1, 1),
            department_id=department.id
        )
        employee.save()

        self.assertEqual(employee.data, {
            "Firstname": employee.first_name,
            "Lastname": employee.last_name,
            "Salary": employee.salary,
            "Birthdate": str(employee.birthdate),
            "Department": department.name
        })
