"""Test for Views and all webservice routes"""
import unittest
from datetime import datetime
# third
from sqlalchemy import create_engine
# own
from app import create_app, db
from models.department import Department
from models.employee import Employee


class ViewsCaseTest(unittest.TestCase):
    """Test case for urls and Views for them"""
    def setUp(self):
        """Initial setup that sets TEST config and path to sqlite3
        db in memory"""
        app = create_app()
        engine = create_engine('sqlite://')

        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = engine.url
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        """Clean database data"""
        for department in Department.get_all():
            department.delete()
        for employee in Employee.get_all():
            employee.delete()
        db.drop_all()
        db.create_all()

    def test_depatments_view_get(self):
        """Test url for DepartmentsView"""
        response = self.app.get('/')
        self.assertEqual(response.status, '200 OK')

    def test_depatment_view_get(self):
        """Test url for DepartmentView"""
        department_name = "Testing department"
        department = Department(name=department_name)
        department.save()

        response = self.app.get(f'/departments/{department_name}')
        self.assertEqual(response.status, '200 OK')

    def test_add_department_view_get(self):
        """Test url for AddDepartmentView"""
        response = self.app.get('/departments/add_department/')
        self.assertEqual(response.status, '200 OK')

    def test_edit_department_view_get(self):
        """Test url for EditDepartmentView"""
        department_name = "Testing department"
        department = Department(name=department_name)
        department.save()

        response = self.app.get(
            '/departments/edit_department/'
            f'{department_name}')
        self.assertEqual(response.status, '200 OK')

    def test_employees_view_get(self):
        """Test url for EmployeesView"""
        response = self.app.get('/employees')
        self.assertEqual(response.status, '200 OK')

    def test_employees_bybirthdate_view_get(self):
        """Test url for EmployeesByBirthdateView"""
        response = self.app.get('/employees/by_birthdate/20000101-200040101')
        self.assertEqual(response.status, '200 OK')

    def test_employee_view_get(self):
        """Test url for EmployeeView"""
        employee = Employee(
            first_name='Test', last_name='Test', salary=18000,
            birthdate=datetime(2000, 1, 1))
        employee.save()

        response = self.app.get(
            f'/employees/{employee.first_name} '
            f'{employee.last_name}')
        self.assertEqual(response.status, '200 OK')

    def test_addemployee_view_get(self):
        """Test url for AddEmployeeView"""
        response = self.app.get('/employees/add_employee/')
        self.assertEqual(response.status, '200 OK')

    def test_editemployee_view_get(self):
        """Test url for EditEmployeeView"""
        employee = Employee(
            first_name='Test', last_name='Test', salary=18000,
            birthdate=datetime(2000, 1, 1))
        employee.save()

        response = self.app.get(
            '/employees/edit_employee/'
            f'{employee.first_name} {employee.last_name}')
        self.assertEqual(response.status, '200 OK')
