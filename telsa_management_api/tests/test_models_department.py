"""Test for SQLALCHEMY.Model instance - Department"""
import unittest
from datetime import datetime
# third
from sqlalchemy import create_engine
# own
from app import create_app, db
from models.department import Department
from models.employee import Employee


class DepartmentModelTestCase(unittest.TestCase):
    """Tests for Department model"""
    def setUp(self):
        """Initial setup that sets TEST config and path to sqlite3
        db in memory"""
        app = create_app()
        engine = create_engine('sqlite://')

        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = engine.url
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        """Clean database data"""
        for department in Department.get_all():
            department.delete()
        for employee in Employee.get_all():
            employee.delete()
        db.drop_all()
        db.create_all()

    def test_create_department(self):
        """Test create Department object in Database"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()

        self.assertEqual(dep_name, department.name)

    def test_get_by_id(self):
        """Test get_by_id class method of Department Model"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()

        self.assertEqual(Department.get_by_id(1), department)

    def test_by_name(self):
        """Test get_name class method of Department Model"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()

        self.assertEqual(Department.get_by_name(dep_name), department)

    def test_get_all(self):
        """Test get_all class method of Department Model"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()

        self.assertEqual(department.get_all()[0], department)

    def test_data_property(self):
        """Test data property method of Department Model"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()

        self.assertEqual(department.data, {
            'Name': dep_name,
            'Average Salary': None,
            'Employees Amount': None
        })

    def test_set_avrg_salary(self):
        """Test set_avrg_salary"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()
        department.set_avrg_salary()
        self.assertEqual(department.avrg_salary, None)

        #  now test avrg_salary with employee in department
        employee = Employee(
            first_name='Test', last_name='Test', salary=18000,
            birthdate=datetime(2002, 12, 1), department_id=department.id)
        employee.save()
        department.set_avrg_salary()
        self.assertEqual(department.avrg_salary, employee.salary)

    def test_set_employees_amount(self):
        """Test set_employees_amount"""
        dep_name = 'Test'
        department = Department(name=dep_name)
        department.save()
        department.set_employees_amount()
        self.assertEqual(department.employees_amount, None)

        #  now test set_employees_amount with employee in department
        employee = Employee(
            first_name='Test', last_name='Test', salary=18000,
            birthdate=datetime(2002, 12, 1), department_id=department.id)
        employee.save()
        department.set_employees_amount()
        self.assertEqual(department.employees_amount, 1)
