"""Test REST classes for Deparment"""
import unittest
import json
from datetime import datetime
from sqlalchemy import create_engine
# own
from app import create_app, db
from models.department import Department
from models.employee import Employee


class DepartmentRESTTestCase(unittest.TestCase):
    """Test REST DepartmentsResource methods"""
    def setUp(self):
        """Initial setup that sets TEST config and path to sqlite3
        db in memory"""
        app = create_app()
        engine = create_engine('sqlite://')

        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = engine.url
        self.app = app.test_client()
        db.drop_all()
        db.create_all()

        # create Test department and Test employee
        self.department = Department(name='Test')
        self.department.save()
        self.employee = Employee(
            first_name='Test',
            last_name='Test',
            salary=18000,
            birthdate=datetime(2000, 1, 1),
            department_id=self.department.id
        )
        self.employee.save()
        self.department.set_avrg_salary()
        self.department.set_avrg_salary()

        self.assertEqual(app.debug, False)

    def tearDown(self):
        """Clean database data"""
        for department in Department.get_all():
            department.delete()
        for employee in Employee.get_all():
            employee.delete()

        db.drop_all()
        db.create_all()

    def test_departments_get(self):
        """Test GET request for DepartmentsResource"""
        response = self.app.get('/api/departments')
        rs_data = json.loads(response.data)['data'][0]
        self.assertEqual(rs_data, self.department.data)

    def test_departments_post(self):
        """Test POST request for DepartmentsResource"""
        # test response if no data given
        response = self.app.post('/api/departments')
        rs_data = json.loads(response.data)
        self.assertTrue('error' in rs_data)

        # test response if no deparment's name was given
        response = self.app.post(
            '/api/departments', json={}
        )
        rs_data = json.loads(response.data)
        self.assertTrue('error' in rs_data)

        # test response if department with such name exists
        response = self.app.post(
            '/api/departments', json={'Name': f'{self.department.name}'}
        )
        rs_data = json.loads(response.data)
        self.assertTrue('error' in rs_data)

        # test response if everythong is ok
        data = {'Name': 'New Test'}
        response = self.app.post(
            '/api/departments', json=data
        )
        rs_data = json.loads(response.data)
        self.assertEqual(rs_data['Name'], data['Name'])

    def test_department_get(self):
        """Test GET request of DepartmentResource"""
        response = self.app.get(
            '/api/departments/TEST_DEPARTMENT'
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test if request is ok
        response = self.app.get(
            f'/api/departments/{self.department.name}'
        )
        self.assertEqual(response.status, '200 OK')

    def test_department_put(self):
        """Test PUT request for DepartmentResource"""
        # test if no data given
        response = self.app.put(
            f'/api/departments/{self.department.name}'
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        #  test if department name doesn't exist in the db
        data = {}
        response = self.app.put(
            '/api/departments/TEST_DEPARTMENT',
            json=json.dumps(data)
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test if attempt to rename department with existing department name
        new_department = Department(name="TEST_DEPARTMENT")
        new_department.save()

        data = {"Name": "TEST_DEPARTMENT"}
        response = self.app.put(
            f'/api/departments/{self.department.name}',
            json=data
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test in case everything is ok
        data = {"Name": "New Test"}
        response = self.app.put(
            f'/api/departments/{self.department.name}',
            json=data
        )
        self.assertEqual(response.status, '200 OK')

    def test_department_delete(self):
        """Test DELETE request for DepartmentResource"""
        # test if no data given
        response = self.app.delete(
            f'/api/departments/{self.department.name}'
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        #  test if department name doesn't exist in the db
        data = {}
        response = self.app.delete(
            '/api/departments/TEST_DEPARTMENT_NEW',
            json=json.dumps(data)
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test if everythong is ok
        data = {}
        response = self.app.delete(
            f'/api/departments/{self.department.name}',
            json=json.dumps(data)
        )
        self.assertEqual(response.status, '204 NO CONTENT')
