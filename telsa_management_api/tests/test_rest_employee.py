"""Test REST classes for Employee"""
import unittest
import json
from datetime import datetime
from sqlalchemy import create_engine
# own
from app import create_app, db
from models.department import Department
from models.employee import Employee


class EmployeeRestTestCase(unittest.TestCase):
    """Test REST Resources for Employee"""
    def setUp(self):
        """Initial setup that sets TEST config and path to sqlite3
        db in memory"""
        app = create_app()
        engine = create_engine('sqlite://')

        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = engine.url
        self.app = app.test_client()
        db.drop_all()
        db.create_all()

        # create Test department and Test employee
        self.department = Department(name='Test')
        self.department.save()
        self.employee = Employee(
            first_name='Test',
            last_name='Test',
            salary=18000,
            birthdate=datetime(2000, 1, 1),
            department_id=self.department.id
        )
        self.employee.save()
        self.department.set_avrg_salary()
        self.department.set_avrg_salary()

        self.assertEqual(app.debug, False)

    def tearDown(self):
        """Clean database data"""
        for department in Department.get_all():
            department.delete()
        for employee in Employee.get_all():
            employee.delete()

        db.drop_all()
        db.create_all()

    def test_employees_get(self):
        """Test GET request for EmployeesResource"""
        response = self.app.get('/api/employees')
        self.assertEqual(response.status, '200 OK')

    def test_employees_post(self):
        """Test POST request for EmployeesResource"""
        # test if no data given
        response = self.app.post(
            '/api/employees'
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test if no data in json
        response = self.app.post(
            '/api/employees',
            json={}
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test attempt to post employee that exists
        response = self.app.post(
            '/api/employees',
            json=self.employee.data
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test if wrong birthdate data given
        response = self.app.post(
            '/api/employees',
            json={
                "Firstname": "Test 1",
                "Lastname": "Test 1",
                "Salary": 18000,
                "Birthdate": "Thirteen",
                "Department": self.department.name
            }
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test if department exists
        response = self.app.post(
            '/api/employees',
            json={
                "Firstname": "Test 1",
                "Lastname": "Test 1",
                "Salary": 18000,
                "Birthdate": "20000101",
                "Department": "New Test"
            }
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        #  test converting data
        response = self.app.post(
            '/api/employees',
            json={
                "Firstname": "Test 1",
                "Lastname": "Test 1",
                "Salary": "thirteen thousands",
                "Birthdate": "20000101",
                "Department": self.department.name
            }
        )
        rs_data = json.loads(response.data)
        self.assertTrue("error" in rs_data)

        # test is post request ok
        response = self.app.post(
            '/api/employees',
            json={
                "Firstname": "Test 1",
                "Lastname": "Test 1",
                "Salary": 13000,
                "Birthdate": "20000101",
                "Department": self.department.name
            }
        )
        rs_data = json.loads(response.data)
        self.assertEqual(response.status, '201 CREATED')

    def test_employee_get(self):
        """Test GET request for EmployeeResource"""
        # test if employee doesn't exist
        response = self.app.get(
            '/api/employees/test1_test1'
        )
        self.assertEqual(response.status, '400 BAD REQUEST')

        # test is request is ok
        response = self.app.get(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}'
        )
        self.assertEqual(response.status, '200 OK')

    def test_employee_put(self):
        """Test PUT request for EmployeeResouece"""
        response = self.app.put(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}'
        )
        self.assertEqual(response.status, '400 BAD REQUEST')

        # test is employee exists
        response = self.app.put(
            '/api/employees/test1_test1',
            json={}
        )
        self.assertEqual(response.status, '404 NOT FOUND')

        # test if attempt to rename employee with name that exists
        employee = Employee(
            first_name='Test1',
            last_name='Test1',
            salary=10000,
            birthdate=datetime(2000, 1, 1),
            department_id=self.department.id
        )
        employee.save()
        response = self.app.put(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}',
            json={
                "Firstname": employee.first_name,
                "Lastname": employee.last_name
            }
        )
        self.assertEqual(response.status, '400 BAD REQUEST')

        # test if wrong salary data type given
        response = self.app.put(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}',
            json={"Salary": "Thirteen thousands"}
        )
        self.assertEqual(response.status, '400 BAD REQUEST')

        # test if worn birthdate given
        response = self.app.put(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}',
            json={"Birthdate": "Twenty years"}
        )
        self.assertEqual(response.status, '400 BAD REQUEST')

        # test if department doesn't exists
        response = self.app.put(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}',
            json={"Department": "Some Department"}
        )
        self.assertEqual(response.status, '404 NOT FOUND')

        # test if request is ok
        response = self.app.put(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}',
            json={
                "Firstname": "Test1",
                "Lastname": "Test2",
                "Salary": 10000,
                "Birthdate": '20000102',
                "Department": "Test"}
        )
        self.assertEqual(response.status, '200 OK')

    def test_employee_delete(self):
        """Test DELETE request for EmployeeResource"""
        # test if employee exists
        response = self.app.delete(
            '/api/employees/some_test'
        )
        self.assertEqual(response.status, '404 NOT FOUND')

        # test if delete is successfull
        response = self.app.delete(
            f'/api/employees/{self.employee.first_name}_'
            f'{self.employee.last_name}'
        )
        self.assertEqual(response.status, '204 NO CONTENT')

    def test_by_birthdate_get(self):
        """Test GET request for EmployeesByBirthdateResource"""
        # test if wrong birthdate given
        response = self.app.get(
            '/api/employees/by_birthdate/thirteen-2020'
        )
        self.assertEqual(response.status, '400 BAD REQUEST')

        # test if reques is ok
        birthdate = ''.join(str(self.employee.birthdate).split('-'))
        response = self.app.get(
            f'/api/employees/by_birthdate/{birthdate}-{birthdate}'
        )
        self.assertEqual(response.status, '200 OK')

    def test_employees_department_get(self):
        """Test GET request for EmployeesDepartmentResource"""
        # test if department doesn't exist
        response = self.app.get(
            '/api/employees/department/Test1'
        )
        self.assertEqual(response.status, '404 NOT FOUND')

        # test if request is ok
        response = self.app.get(
            f'/api/employees/department/{self.department.name}'
        )
        self.assertEqual(response.status, '200 OK')
