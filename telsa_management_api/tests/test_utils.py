"""Test for functions in Utils module"""
import unittest
from datetime import date
# own
from utils.utils import is_wrong_birthdate


class IsWrongBirthdayTestCase(unittest.TestCase):
    """Test functions in is_wrong_birthdate"""
    def test_birthdate_length(self):
        """Test if birthdate length is correct"""
        birthdate = '200001010'
        self.assertTrue(is_wrong_birthdate(birthdate))

    def test_incorrect_birthdate(self):
        """Test normal birthdate format"""
        birthdate = '20000101'
        self.assertFalse(is_wrong_birthdate(birthdate))

        current_year = date.today().year
        year = current_year - 20
        month = 12
        day = 12
        self.assertTrue(is_wrong_birthdate(f'{current_year}{month}{day}'))
        self.assertTrue(is_wrong_birthdate(f'{current_year-1}{month}{day}'))
        self.assertTrue(is_wrong_birthdate(f'{year}{month}{40}'))
        self.assertTrue(is_wrong_birthdate(f'{year}{13}{day}'))
        self.assertTrue(is_wrong_birthdate(f'{1999}02{29}'))
