"""Module that has different util functions used across app"""
from re import sub
from datetime import datetime


def is_wrong_birthdate(birthdate_str):
    """Checks given birthdate string.

    :return:-:False or str: if birthdate is correct returns None,
    else error message"""
    # check given data types and data compliance patterns
    # check if birthdate is of right pattern (year-month-day)
    birthdate = sub(r'[^0-9]', '', str(birthdate_str))
    nums_in_date = 8
    error = None
    current_year = datetime.today().timetuple()[0]
    pattern = "Birthdate must be in 'year-month-day' pattern"

    if not len(birthdate) == nums_in_date:
        error = {
            "error": f"given wrong birthdate: '{birthdate_str}'. {pattern}"}
        return error

    year = int(birthdate[0:4])
    month = int(birthdate[4:6])
    day = int(birthdate[6:8])

    # checks age of employee
    if not year < current_year:
        error = {
            "error": f"given wrong birhtday year: {year}"
        }
        return error
    elif current_year - year < 16:
        error = {
            "error": f"employee with birthdate {year}-{month}-{day}"
            " would be too young to work!"
        }
        return error
    # check day in birthdate
    elif not 0 < day <= 31:
        error = {
            "error": f"given wrong day in birthdate: {day}. {pattern}"
        }
        return error
    # check month of birthdate
    elif not 0 < month <= 12:
        error = {
            "error": f"given wrong month in birthdate: {month}."
            f" There is no {month} months in year"
        }
        return error
    elif month == 2 and day > 28 and year % 4 != 0 or month == 2 and day > 29:
        error = {
            "error": f"given wrong day in birthdate: {day}."
            f"There is no {day} days in February."
        }
        return error

    return error
