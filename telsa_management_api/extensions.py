"""Module for adding extensions for Flask instance:
SQLAlchemy, etc"""
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
