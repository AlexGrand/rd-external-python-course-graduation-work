"""Module that works with requests from website.
View functions get request method and render template"""
from flask import render_template
from flask.views import MethodView


class DepatmentsView(MethodView):
    """View that renders main page with list of departments"""
    def get(self):
        """Catch GET request and render template"""
        return render_template('departments.html')
