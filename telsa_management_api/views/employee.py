"""Module that works with requests from website.
View functions get request method and render template"""
from flask import render_template
from flask.views import MethodView


class EmployeeView(MethodView):
    """View that renders employee page"""
    def get(self, employee_name):
        """Catch GET request and render template"""
        return render_template('employee.html', employee_name=employee_name)


class AddEmployeeView(MethodView):
    """View that renders new employee page"""
    def get(self):
        """Catch GET request and render template"""
        return render_template('add_employee.html')


class EditEmployeeView(MethodView):
    """View that renders edit department page"""
    def get(self, employee_name):
        """Catch GET request and render template"""
        return render_template(
            'edit_employee.html', employee_name=employee_name)
