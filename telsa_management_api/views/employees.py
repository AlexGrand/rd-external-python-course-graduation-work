"""Module that works with requests from website.
View functions get request method and render template"""
from flask import render_template
from flask.views import MethodView


class EmployeesView(MethodView):
    """View that renders main page with list of employees"""
    def get(self):
        """Catch GET request and render template"""
        return render_template('employees.html')


class EmployeesByBirthdateView(MethodView):
    """View that renders page with list of employees by given
    birthdate range"""
    def get(self, min_date, max_date):
        """Catch GET request and render template"""
        return render_template(
            'employees_by_birthdate.html',
            min_date=min_date, max_date=max_date)
