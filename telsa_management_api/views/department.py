"""Module that works with requests from website.
View functions get request method and render template"""
from flask import render_template
from flask.views import MethodView


class DepatmentView(MethodView):
    """View that renders department page with employees on it"""
    def get(self, department_name):
        """Catch GET request and render template"""
        return render_template(
            'department.html', department_name=department_name)


class AddDepatmentView(MethodView):
    """View that renders new department page"""
    def get(self):
        """Catch GET request and render template"""
        return render_template('add_department.html')


class EditDepartmentView(MethodView):
    """View that renders edit department page"""
    def get(self, department_name):
        """Catch GET request and render template"""
        return render_template(
            'edit_department.html', department_name=department_name)
