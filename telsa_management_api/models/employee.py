"""Employee Model is SQLAlchemy model that works with
employee table in the database."""

from extensions import db
from models.department import Department


class Employee(db.Model):
    """SQLAlchemy.Model that creates, edit and delete Employee table
    and data in the database."""
    __tablename__ = 'employee'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20), nullable=False)
    last_name = db.Column(db.String(35), nullable=False)
    salary = db.Column(db.Float, nullable=False)
    birthdate = db.Column(db.Date, nullable=False)
    department_id = db.Column(db.Integer(), db.ForeignKey("department.id"))

    @classmethod
    def get_by_id(cls, employee_id):
        """Employee Class method to find employee by id.

        :employee_id: - employee's id in the database

        :return: Employee object"""
        return cls.query.filter_by(id=employee_id).first()

    @classmethod
    def get_by_name(cls, first_name, last_name):
        """Employee Class method to find employee by id.

        :firstname: - employee's firstname in the database
        :lastname: - employee's lastname in the database

        :return: Department object"""
        return cls.query.filter(
            Employee.first_name == first_name,
            Employee.last_name == last_name).first()

    @classmethod
    def get_by_birthdate_range(cls, min_date, max_date):
        """Employee Class method to find employee in range of given birthdates.

        :min_date: - :str: - min birthdate
        :max_date: - :str: - max birthdate

        :return: - :list: - list of all occurrences"""
        return cls.query.filter(
            cls.birthdate.between(min_date, max_date)).all()

    @classmethod
    def get_all(cls):
        """Get all employee in the database.

        :return: list of employees"""
        return cls.query.all()

    @property
    def data(self):
        """Get all Employee data in dict type"""
        department = Department.get_by_id(self.department_id)
        if department:
            department = department.name
        return {
            'Firstname': self.first_name,
            'Lastname': self.last_name,
            'Salary': self.salary,
            'Birthdate': str(self.birthdate),
            'Department': department
        }

    def save(self):
        """Public method to save Employee data to the Employee table"""
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Public method to delete Employee data from the Employee table"""
        db.session.delete(self)
        db.session.commit()
