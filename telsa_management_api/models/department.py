"""Department Model is SQLAlchemy model that works with
department table in the database."""

from extensions import db


class Department(db.Model):
    """SQLAlchemy.Model that creates, edit and delete table
    and data in the database."""
    __tablename__ = 'department'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    avrg_salary = db.Column(db.Float)
    employees_amount = db.Column(db.Integer)
    employees = db.relationship('Employee', backref='employee')

    @classmethod
    def get_by_id(cls, department_id):
        """Department Class method to find department by id.

        :department_id: - department's id in the database

        :return: Department object"""
        return cls.query.filter_by(id=department_id).first()

    @classmethod
    def get_by_name(cls, name):
        """Department Class method to find department by id.

        :dep_name: - department's name in the database

        :return: Department object"""
        return cls.query.filter_by(name=name).first()

    @classmethod
    def get_all(cls):
        """Get all departments in the database.

        :return: list of departments"""
        return cls.query.all()

    @property
    def data(self):
        """Get all Department data in dict type"""
        return {
            'Name': self.name,
            'Average Salary': self.avrg_salary,
            'Employees Amount': self.employees_amount
        }

    def save(self):
        """Public method to save data to the database"""
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Public method to delete data from the database"""
        db.session.delete(self)
        db.session.commit()

    def set_avrg_salary(self):
        """Set average salary for department.

        This method must be called on each POST, PUT, GET request
        to Employee to work correctly"""
        avrg_salary = None
        if len(self.employees) > 0:
            total_salary = sum(empl.salary for empl in self.employees)
            avrg_salary = total_salary / len(self.employees)

        self.avrg_salary = avrg_salary
        self.save()

    def set_employees_amount(self):
        """Count employees amount.

        This method must be called on each POST, PUT, GET request
        to Employee to work correctly"""
        amount = None
        if len(self.employees) > 0:
            amount = len(self.employees)
        self.employees_amount = amount
        self.save()
