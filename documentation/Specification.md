# **SOFTWARE SPECIFICATION REQUIREMENTS**


# TABLE OF CONTENTS
1. [Introduction](#1-introduction)\
1.1. [Purpose](#11-purpose)\
1.2. [Document Conventions](#12-document-conventions)\
1.3. [Project Scope](#13-project-scope)
2. [Overall Desription](#2-overall-desription)\
2.1. [Database Perspective](#21-database-perspective)\
2.2. [Product Perspective and Functionality](#22-product-perspective-and-functionality)\
2.3. [Operating environment](#23-operating-environment)\
2.4. [References](#24-references)
3. [System Features](#3-system-features)\
3.1. [User interfaces:](#31-user-interfaces)\
3.2. [Hardware interfaces](#32-hardware-interfaces)\
3.3. [Software interfaces](#33-software-interfaces)
4. [Testing, dosctrings and development progress](#4-testing-dosctrings-and-development-progress)

# 1. INTRODUCTION

* ## 1.1 PURPOSE

The purpose of this document is to build an online system to store and edit information about departments and employees in the Telsa Inc (American electric vehicle and clean energy company).

* ## 1.2 Document Conventions

This document uses the following conventions.

`DB` - Database\
`table` - Database table

* ## 1.3 Project Scope

The purpose of the online departments management system is to ease company's inner personal management system and to create a convenient and easy-to-use application for HRs and heads of departments. The system is based on a relational database and is able to store, list, add, remove, edit or update information about all existing departments in the company and its employees.

# 2. Overall Desription

* ## 2.1 Database Perspective

A personal management database system stores the following information.

> **Department details:**\
The departmant table will include information about all existing employees in the department, department's name, average salary, employees amount in the department.

> **Employee details:**
The employee table will include information about each employee, name, date of birth, salary, id of the department

> **User details:**
The user table is created for stofing information about list of users who are able to add, edit or delete information in Employee and Department tables.
User table will include user's name and hashed password. Possibly added features - granting special permissions for each add/edit/delete option.


* ## 2.2 Product Perspective and Functionality

**Functionality for registered and unregistered users:**

Registered user | Unregistered user
------------------|-----------------
Add, update, view, delete information about each department and employee;<br><br>Search user by name, date of birth<br> | View information about each department and employee;<br><br>Search user by name, date of birth

* ## 2.3 Operating environment

`database` - MySQl\
`operating system` - Linux Server\
`language` - Python3.8.5+\
`webframework` - Flask\
`DB Object-relational mapping` - sqlalchemy\
`REST functionality` - flask-restful

* ## 2.4 References

[PostgreSQL](https://www.postgresql.org/)\
[Flask](https://flask.palletsprojects.com/en/1.1.x/)\
[Flask restful](https://flask-restful.readthedocs.io/en/latest/)\
[Flask sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)


# 3. System Features

* ## 3.1 User interfaces:
1. Front-end software: html, css, js
2. Back-end software: python, mysql

* ## 3.2 Hardware interfaces
1. Linux server/heroku system
2. A browser which supports HTML & CSS3

* ## 3.3 Software interfaces
1. WEB-service interface using browser
2. Terminal, Postman for CRUD operations

# 4. Testing, dosctrings and development progress

During application development will be used so-called test-driven approach. I gonna use `unittest` and `tempfile` modules for testing each function after it implementation. Each public function and class will have docstring comments with short description, params and return values. `types` annotations will be used on each public function.

Debug mode will be available during the whole development process.


>### Furthemore specifications and requirements can be added during development process

